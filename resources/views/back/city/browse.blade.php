@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('city.create') }}" class="btn btn-primary">Add City</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Cities</span></div>
                    <div class="card-body">
                        @include('back.partials.message')
                        <div class="container-fluid">
                                @if(count($cities)>0)
                                    @foreach($cities as $city)
                                        <div class="row my-3">
                                            <div class="col-4">
                                                <img src="{{ asset('storage/'.$city->image) }}" alt="{{ $city->name }}" class="img-fluid">
                                            </div>
                                            <div class="col-8">
                                                <h3>{{ $city->name }}</h3>
                                                <p>{{ $city->description }}</p>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('city.edit', $city->id) }}" class="btn btn-warning btn-block">Edit</a>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('city.destroy', $city->id) }}" class="btn btn-danger btn-block">Delete</a>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <p class="card-text">No city found!</p>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
