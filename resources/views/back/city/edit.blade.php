@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('city.browse') }}" class="btn btn-primary">List Cities</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Edit City</span></div>
                    <div class="card-body">
                        <form action="{{ route('city.update', $city->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" class="form-control" name="name" placeholder="Enter the city name..." value="{{ $city->name }}">
                            </div>
                            <div class="form-group">
                                <label for="timezone">Timezone in GMT</label>
                                <input type="text" id="timezone" class="form-control" name="timezone" placeholder="GMT+6" value="{{ $city->timezone }}">
                            </div>
                            <div class="form-group">
                                <label for="country">Country</label>
                                <input type="text" id="country" class="form-control" name="country" placeholder="Bangladesh" value="{{ $city->country }}">
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ $city->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="image">City Landscape/Photo</label>
                                <input type="file" id="image" class="form-control-file" name="image">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="submit" role="button">Update</button>
                                <a href="{{ route('city.browse') }}" class="btn btn-warning">Go Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
