@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('school.browse') }}" class="btn btn-primary">List Schools</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Schools</span></div>
                    <div class="card-body">
                        <form action="{{ route('school.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" class="form-control" name="name" placeholder="Enter the school name..." value="{{ old('name') }}">
                            </div>
                            <div class="form-group">
                                <label for="code">School Short Name</label>
                                <input type="text" id="code" class="form-control" name="code" placeholder="" value="{{ old('code') }}">
                            </div>
                            <div class="form-group">
                                <label for="address">Address</label>
                                <input type="text" id="address" class="form-control" name="address" placeholder="Enter your address..." value="{{ old('address') }}">
                            </div>
                            <div class="form-group">
                                <label for="city_id">City</label>
                                <select name="city_id" id="city_id" class="custom-select">
                                    @if(count($cities)>0)
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="submit" role="button">Create</button>
                                <a href="{{ route('city.browse') }}" class="btn btn-warning">Go Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
