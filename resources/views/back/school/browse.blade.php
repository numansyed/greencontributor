@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('school.create') }}" class="btn btn-primary">Add School</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Schools</span></div>
                    <div class="card-body">
                        @include('back.partials.message')
                        <div class="container-fluid">
                                @if(count($schools)>0)
                                    @foreach($schools as $school)
                                        <div class="row my-3">
                                            <div class="col-12">
                                                <h3>{{ $school->name }} - {{ $school->code }}</h3>
                                                <p>{{ $school->address }}</p>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('school.edit', $school->id) }}" class="btn btn-warning btn-block">Edit</a>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('school.destroy', $school->id) }}" class="btn btn-danger btn-block">Delete</a>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <p class="card-text">No school found!</p>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
