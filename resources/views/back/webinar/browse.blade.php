@extends('back.layout')
@section('content')
    <div class="container">
        @if(Auth::user()->type != 'teacher')
            <div class="row my-3">
                <div class="col">
                    <a href="{{ route('webinar.create') }}" class="btn btn-primary">Add Event</a>
                </div>
            </div>
        @endif
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Events</span></div>
                    <div class="card-body">
                        @include('back.partials.message')
                        <div class="container-fluid">
                                @if(count($webinars)>0)
                                    @foreach($webinars as $webinar)
                                        <div class="row my-3">
                                            <div class="col-12">
                                                <h3>{{ $webinar->title }}</h3>
                                                <a href="{{ $webinar->join_link }}" target="_blank">Join Webinar</a>
                                                <p>{{ $webinar->description }}</p>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('webinar.edit', $webinar->id) }}" class="btn btn-warning btn-block">Edit</a>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('webinar.destroy', $webinar->id) }}" class="btn btn-danger btn-block">Delete</a>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <p class="card-text">No webinar found!</p>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
