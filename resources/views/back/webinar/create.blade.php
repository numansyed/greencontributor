@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('webinar.browse') }}" class="btn btn-primary">List Events</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Add Event</span></div>
                    <div class="card-body">
                        @include('back.partials.message')
                        <form action="{{ route('webinar.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" id="title" class="form-control" name="title" placeholder="Enter the title for the event/webinar..." value="{{ old('title') }}">
                            </div>
                            <div class="form-group">
                                <label for="city_id">City</label>
                                <select name="city_id" id="city_id" class="custom-select">
                                    @foreach($cities as $city)
                                        <option value="{{ $city->id }}"><span class="text-capitalize">{{ ucwords($city->name) }}</span></option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="status">Status:</label>
                                <select name="status" id="status" class="custom-select">
                                    <option value="tbc">TBC</option>
                                    <option value="tba">TBA</option>
                                    <option value="confirmed">Confirmed</option>
                                    <option value="open">Open</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ old('description') }}</textarea>
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="webinarDate">Date:</label>
                                        <input type="date" name="webinarDate" class="form-control">
                                    </div>
                                    <div class="col-4">
                                        <label for="start_time">Start Time:</label>
                                        <input type="time" name="start_time" class="form-control">
                                    </div>
                                    <div class="col-4">
                                        <label for="end_time">End Time:</label>
                                        <input type="time" name="end_time" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="join_link">Webinar/Event Joining Link</label>
                                <input type="text" value="{{ old('join_link') }}" id="join_link" name="join_link" class="form-control" placeholder="Enter the joining link for the webinar">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="submit" role="button">Submit</button>
                                <a href="{{ route('activity.browse') }}" class="btn btn-warning">Go Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-inline-script')
    <script>
        $("#agreement").on("change", function (){
            console.log($("#agreement").checked)
        })
    </script>
@endsection