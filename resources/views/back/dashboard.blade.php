@extends('back.layout')
@section('content')
    <div class="container mt-4">
        <div class="row">
            <div class="col-2">
                <div class="card shadow">
                    <div class="card-header"><div class="card-title text-center">Cities</div></div>
                    <div class="card-body">
                        <h2 class="card-text text-center">{{ count($cities) }}</h2>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="card shadow">
                    <div class="card-header"><div class="card-title text-center">Schools</div></div>
                    <div class="card-body">
                        <h2 class="card-text text-center">{{ count($schools) }}</h2>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="card shadow">
                    <div class="card-header"><div class="card-title text-center">Teachers</div></div>
                    <div class="card-body">
                        <h2 class="card-text text-center">{{ count($teachers) }}</h2>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="card shadow">
                    <div class="card-header"><div class="card-title text-center">Students</div></div>
                    <div class="card-body">
                        <h2 class="card-text text-center">{{ count($students) }}</h2>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="card shadow">
                    <div class="card-header"><div class="card-title text-center">Activities</div></div>
                    <div class="card-body">
                        <h2 class="card-text text-center">{{ count($activities) }}</h2>
                    </div>
                </div>
            </div>
            <div class="col-2">
                <div class="card shadow">
                    <div class="card-header"><div class="card-title text-center">Events</div></div>
                    <div class="card-body">
                        <h2 class="card-text text-center">{{ count($webinars) }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
