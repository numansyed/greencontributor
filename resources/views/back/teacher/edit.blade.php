@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('teacher.browse') }}" class="btn btn-primary">List Teachers</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Teachers</span></div>
                    <div class="card-body">
                        <form action="{{ route('teacher.update', $teacher->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" class="form-control" name="name" placeholder="Enter the teacher name..." value="{{ $teacher->name }}">
                            </div>
                            <div class="form-group">
                                <label for="user_id">Teachers Code</label>
                                <input type="text" id="user_id" class="form-control" name="user_id" placeholder="" value="{{ $teacher->user_id }}">
                            </div>
                            <div class="form-group">
                                <label for="gender">Gender</label>
                                <select name="gender" id="gender" class="custom-select">
                                    <option value="male" {{ $teacher->gender == 'male' ? 'selected' : '' }}>Male</option>
                                    <option value="female" {{ $teacher->gender == 'female' ? 'selected' : '' }}>Female</option>
                                    <option value="other" {{ $teacher->gender == 'other' ? 'selected' : '' }}>Other</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" id="email" name="email" class="form-control" placeholder="Enter teacher's email" value="{{ $teacher->email }}">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col">
                                        <label for="password">Password</label>
                                        <input type="password" name="password" class="form-control" id="password">
                                    </div>
                                    <div class="col">
                                        <label for="confirm_password">Confirm Password</label>
                                        <input type="password" name="confirm_password" class="form-control" id="confirm_password">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="city_id">City</label>
                                <select name="city_id" id="city_id" class="custom-select">
                                    @if(count($cities)>0)
                                        @foreach($cities as $city)
                                            <option value="{{ $city->id }}" {{ $teacher->city_id == $city->id ? 'selected' : '' }}>{{ $city->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="school_id">School</label>
                                <select name="school_id" id="school_id" class="custom-select">
                                    @if(count($schools)>0)
                                        @foreach($schools as $school)
                                            <option value="{{ $school->id }}" {{ $teacher->school_id == $school->id ? 'selected' : '' }}>{{ $school->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="submit" role="button">Update</button>
                                <a href="{{ route('teacher.browse') }}" class="btn btn-warning">Go Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
