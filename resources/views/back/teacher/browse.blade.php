@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('teacher.create') }}" class="btn btn-primary">Add Teacher</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Teachers</span></div>
                    <div class="card-body">
                        @include('back.partials.message')
                        <div class="container-fluid">
                                @if(count($teachers)>0)
                                    @foreach($teachers as $teacher)
                                        <div class="row my-3">
                                            <div class="col-12">
                                                <h3>{{ $teacher->name }} - {{ $teacher->user_id }}</h3>
                                                <p>{{ $teacher->school->name }}</p>
                                                <p>{{ $teacher->city->name }}</p>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('teacher.edit', $teacher->id) }}" class="btn btn-warning btn-block">Edit</a>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('teacher.destroy', $teacher->id) }}" class="btn btn-danger btn-block">Delete</a>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <p class="card-text">No teacher found!</p>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
