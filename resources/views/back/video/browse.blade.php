@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('video.create') }}" class="btn btn-primary">Add Video</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Videos</span></div>
                    <div class="card-body">
                        @include('back.partials.message')
                        <div class="container-fluid">
                                @if(count($videos)>0)
                                    @foreach($videos as $video)
                                        <div class="row my-3">
                                            <div class="col-4">
                                                <div class="embed-responsive embed-responsive-16by9">
                                                    <video src="{{ asset($video->link) }}" class="embed-responsive-item" controls="true"></video>
                                                </div>
                                            </div>
                                            <div class="col-8">
                                                <h3>{{ $video->title }}</h3>
                                                <p>{{ $video->description }}</p>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('video.edit', $video->id) }}" class="btn btn-warning btn-block">Edit</a>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('video.destroy', $video->id) }}" class="btn btn-danger btn-block">Delete</a>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <p class="card-text">No videos found!</p>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
