@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('video.browse') }}" class="btn btn-primary">List Videos</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Edit Video</span></div>
                    <div class="card-body">
                        <form action="{{ route('video.update', $video->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" id="title" class="form-control" name="title" placeholder="Enter the video title..." value="{{ $video->title }}">
                            </div>
                            <div class="form-group">
                                <label for="video_category_id">Category</label>
                                <select name="video_category_id" id="video_category_id" class="custom-select">
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ $video->video_category_id == $category->id ? 'selected' : '' }}>{{ $category->title }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ $video->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="video">Video</label>
                                <input type="file" id="video" class="form-control-file" name="video">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="submit" role="button">Create</button>
                                <a href="{{ route('video.browse') }}" class="btn btn-warning">Go Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
