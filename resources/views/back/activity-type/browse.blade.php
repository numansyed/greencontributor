@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('activity-type.create') }}" class="btn btn-primary">Add Activity Master</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Activity Masters</span></div>
                    <div class="card-body">
                        @include('back.partials.message')
                        <div class="container-fluid">
                                @if(count($activityTypes)>0)
                                    @foreach($activityTypes as $activityType)
                                        <div class="row my-3">
                                            <div class="col-12">
                                                <h3>{{ $activityType->title }}</h3>
                                                <p>Points: {{ $activityType->points }}</p>
                                                <p>Has Video: {{ $activityType->hasVideo }}</p>
                                                <p>Has Photo: {{ $activityType->hasPhoto }}</p>
                                                <p>Has Description: {{ $activityType->hasText }}</p>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('activity-type.edit', $activityType->id) }}" class="btn btn-warning btn-block">Edit</a>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('activity-type.destroy', $activityType->id) }}" class="btn btn-danger btn-block">Delete</a>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <p class="card-text">No master activity found!</p>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
