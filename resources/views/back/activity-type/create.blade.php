@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('activity-type.browse') }}" class="btn btn-primary">List Activity Masters</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Add Activity Master</span></div>
                    <div class="card-body">
                        <form action="{{ route('activity-type.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" id="title" class="form-control" name="title" placeholder="Enter the title for Activity Master..." value="{{ old('title') }}">
                            </div>
                            <div class="form-group">
                                <label for="points">Points</label>
                                <input type="number" id="points" class="form-control" name="points" min="0000" max="9999" value="{{ old('points') }}">
                            </div>
                            <div class="form-group">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="hasVideo" id="hasVideo">
                                    <label class="form-check-label" for="hasVideo">
                                        Does the activity type has video?
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="hasPhoto" id="hasPhoto">
                                    <label class="form-check-label" for="hasPhoto">
                                        Does the activity type has photo?
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="hasText" id="hasText">
                                    <label class="form-check-label" for="hasText">
                                        Does the activity type has description?
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="submit" role="button">Create</button>
                                <a href="{{ route('activity-type.browse') }}" class="btn btn-warning">Go Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
