@extends('back.layout')
@section('content')
    <div class="container">
        @if(Auth::user()->type != 'teacher')
            <div class="row my-3">
                <div class="col">
                    <a href="{{ route('activity.create') }}" class="btn btn-primary">Add Activity</a>
                </div>
            </div>
        @endif
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Activities</span></div>
                    <div class="card-body">
                        @include('back.partials.message')
                        <div class="container-fluid">
                                @if(count($activities)>0)
                                    @foreach($activities as $activity)
                                        <div class="row my-3">
                                            <div class="col-12">
                                                <h3>{{ $activity->title }}</h3>
                                                <a href="{{ $activity->video }}" target="_blank">Download Video</a>
                                                @if($activity->video)

                                                @else
                                                    <img src="{{ asset($activity->photo) }}" alt="{{ $activity->title }}" class="img-fluid">
                                                @endif
                                                <p>{{ $activity->description }}</p>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('activity.edit', $activity->id) }}" class="btn btn-warning btn-block">Edit</a>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('activity.destroy', $activity->id) }}" class="btn btn-danger btn-block">Delete</a>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <p class="card-text">No activity found!</p>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
