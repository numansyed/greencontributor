@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('activity.browse') }}" class="btn btn-primary">List Activities</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Edit Activity</span></div>
                    <div class="card-body">
                        <form action="{{ route('activity.update', $activity->id) }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" id="title" class="form-control" name="title" placeholder="Enter the title for Activity Master..." value="{{ $activity->title }}">
                            </div>
                            <div class="form-group">
                                <label for="activity_type_id">Activity Type</label>
                                <select name="activity_type_id" id="activity_type_id" class="custom-select">
                                    @foreach($activityTypes as $activityType)
                                        <option value="{{ $activityType->id }}" {{ $activityType->id == $activity->activity_type_id ? 'selected' : '' }}><span class="text-capitalize">{{ ucwords($activityType->title) }}</span></option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="student_id">Student</label>
                                <select name="student_id" id="student_id" class="custom-select">
                                    @foreach($students as $student)
                                        <option value="{{ $student->id }} {{ $student->id == $activity->student_id ? 'selected' : '' }}"><span class="text-capitalize">{{ ucwords($student->name) }}</span></option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="school_id">School</label>
                                <select name="school_id" id="school_id" class="custom-select">
                                    @foreach($schools as $school)
                                        <option value="{{ $school->id }}" {{ $school->id == $activity->school_id ? 'selected' : '' }}><span class="text-capitalize">{{ ucwords($school->name) }}</span></option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="10" class="form-control">{{ $activity->description }}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="image">Image</label>
                                <input type="file" class="form-control-file" accept="image/*" name="image" id="image">
                            </div>
                            <div class="form-group">
                                <label for="video">For video use the below "We Transfer" link</label>
                                <input type="text" name="video" class="form-control" placeholder="Send your videos first using 'We Transfer' and copy the link here." value="{{ $activity->video }}">
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="submit" role="button">Update</button>
                                <a href="{{ route('activity.browse') }}" class="btn btn-warning">Go Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
