@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('activity.browse') }}" class="btn btn-primary">List Activities</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Activity Upload Form</span></div>
                    <div class="card-body">
                        <form action="{{ route('activity.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input type="text" id="title" class="form-control" name="title" placeholder="Enter the title for Activity Master..." value="{{ old('title') }}">
                            </div>
                            <div class="form-group">
                                <label for="activity_type_id">Activity Type</label>
                                <select name="activity_type_id" id="activity_type_id" class="custom-select">
                                    @foreach($activityTypes as $activityType)
                                        <option value="{{ $activityType->id }}"><span class="text-capitalize">{{ ucwords($activityType->title) }}</span></option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description" id="description" cols="30" rows="10" class="form-control"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="image">Image or Document</label>
                                <input type="file" class="form-control-file" accept="application/msword, application/vnd.ms-excel, application/vnd.ms-powerpoint,
text/plain, application/pdf, image/*" name="image" id="image">
                            </div>
                            <div class="form-group">
                                <label for="video">For video use the below "We Transfer" link</label>
                                <input type="text" name="video" class="form-control" placeholder="Send your videos first using 'We Transfer' and copy the link here.">
                            </div>
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="agreement">
                                    <label class="custom-control-label" for="agreement">
                                        The submission of this document for uploading is with full knowledge of our teacher and parents, who have given the permission and an email is auto generated to the teacher about this activity. We give consent for our video/photo to be published online. We also authorize Greencontributor to make the necessary edits to reflect the video or photograph content for Greencontributor program
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button class="btn btn-success" type="submit" role="button">Submit</button>
                                <a href="{{ route('activity.browse') }}" class="btn btn-warning">Go Back</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-inline-script')
    <script>
        $("#agreement").on("change", function (){
            console.log($("#agreement").checked)
        })
    </script>
@endsection