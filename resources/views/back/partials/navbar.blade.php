<header class="bg-light shadow-lg py-3">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-between">
            <div class="col-auto">
                <a href="{{ route('dashboard') }}" class="navbar-brand"><img src="{{ asset('img/logo.png') }}" alt="{{ config('app.name') }}"> GreenContributor</a>
            </div>
            <div class="col-auto row align-items-center justify-content-end">
                @auth
                    @if(Auth::user()->type == 'student')
                        <div class="col"><a href="{{ route('dashboard') }}">Dashboard</a></div>
                        <div class="col"><a href="{{ route('activity.browse') }}">Activity</a></div>
                        <div class="col"></div>
                        <div class="col">
                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button class="btn" type="submit" role="button">Logout</button>
                            </form>
                        </div>
                    @endif
                    @if(Auth::user()->type == 'teacher')
                        <div class="col"><a href="{{ route('dashboard') }}">Dashboard</a></div>
                        <div class="col"><a href="{{ route('activity.browse') }}">Activity</a></div>
                        <div class="col"></div>
                        <div class="col">
                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button class="btn" type="submit" role="button">Logout</button>
                            </form>
                        </div>
                    @endif
                    @if(Auth::user()->type == 'admin')
                        <div class="col"><a href="{{ route('dashboard') }}">Dashboard</a></div>
                        <div class="col"><a href="{{ route('city.browse') }}">City</a></div>
                        <div class="col"><a href="{{ route('school.browse') }}">School</a></div>
                        <div class="col"><a href="{{ route('teacher.browse') }}">Teacher</a></div>
                        <div class="col-auto"><a href="{{ route('activity-type.browse') }}">Activity Master</a></div>
                        <div class="col"><a href="{{ route('activity.browse') }}">Activity</a></div>
                        <div class="col"><a href="{{ route('webinar.browse') }}">Event</a></div>
                        <div class="col-auto"><a href="{{ route('category.browse') }}">Video Categories</a></div>
                        <div class="col"><a href="{{ route('video.browse') }}">Videos</a></div>
                        <div class="col"></div>
                        <div class="col">
                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <button class="btn" type="submit" role="button">Logout</button>
                            </form>
                        </div>
                    @endif
                @endauth
                @guest
                    <div class="col"><a href="{{ route('login') }}">Login</a></div>
                    <div class="col"><a href="{{ route('register') }}">Register</a></div>
                @endguest
            </div>
        </div>
    </div>
</header>
