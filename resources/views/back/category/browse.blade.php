@extends('back.layout')
@section('content')
    <div class="container">
        <div class="row my-3">
            <div class="col">
                <a href="{{ route('category.create') }}" class="btn btn-primary">Add Video Category</a>
            </div>
        </div>
        <div class="row my-3">
            <div class="col">
                <div class="card">
                    <div class="card-header"><span class="card-title">Video Categories</span></div>
                    <div class="card-body">
                        @include('back.partials.message')
                        <div class="container-fluid">
                                @if(count($categories)>0)
                                    @foreach($categories as $category)
                                        <div class="row my-3">
                                            <div class="col-12">
                                                <h3>{{ $category->title }}</h3>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('category.edit', $category->id) }}" class="btn btn-warning btn-block">Edit</a>
                                            </div>
                                            <div class="col-6 mt-3">
                                                <a href="{{ route('category.destroy', $category->id) }}" class="btn btn-danger btn-block">Delete</a>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    <p class="card-text">No category found!</p>
                                @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
