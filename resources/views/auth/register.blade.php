@extends('back.layout')
@section('content')
    <div class="container mt-4">
        <div class="row justify-content-center">
            <div class="col-10">
                <div class="card">
                    <div class="card-header"><span class="card-title">Register</span></div>
                    <div class="card-body">
                        @include('back.partials.message')
                        <form action="{{ route('register') }}" method="POST">
                            @csrf
                            @if ($errors->any())
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            @endif
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" id="name" name="name" class="form-control" placeholder="Enter your name..." value="{{ old('name') }}">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="text" id="email" name="email" class="form-control" placeholder="Enter your email..." value="{{ old('email') }}">
                            </div>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-2">
                                        <label for="gender">Gender</label>
                                        <select name="gender" id="gender" class="custom-select form-control">
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                            <option value="other">Other</option>
                                        </select>
                                    </div>
                                    <div class="col-6">
                                        <label for="school_id">School</label>
                                        <select name="school_id" id="school_id" class="custom-select form-control">
                                            <option value="0">None</option>
                                            @if(count($schools) > 0)
                                                @foreach($schools as $school)
                                                    <option value="{{ $school->id }}">{{ $school->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <label for="teacher_id">Teacher</label>
                                        <select name="teacher_id" id="teacher_id" class="custom-select form-control">
                                            <option value="0">None</option>
                                            @if(count($teachers) > 0)
                                                @foreach($teachers as $teacher)
                                                    <option value="{{ $teacher->id }}">{{ $teacher->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                    <div class="col-12">
                                        <label for="city_id">City</label>
                                        <select name="city_id" id="city_id" class="custom-select form-control">
                                            <option value="0">None</option>
                                            @if(count($cities) > 0)
                                                @foreach($cities as $city)
                                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                                @endforeach
                                            @endif
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col">
                                            <label for="password">Password</label>
                                            <input type="password" id="password" class="form-control" name="password">
                                        </div>
                                        <div class="col">
                                            <label for="password_confirmation">Confirm Password</label>
                                            <input type="password" id="password_confirmation" class="form-control" name="password_confirmation">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="col">
                                        <button class="btn btn-primary" type="submit" role="button">Register</button>
                                        <p><a href="{{ route('login') }}">Already registered? Login</a></p>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
