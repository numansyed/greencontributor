Your student <strong>{{ $student }}</strong> participating in Greencontributor program has uploaded a video to be sent to Greencontributor for edits to be uploaded  on Greencontributor channel.

This email is a reminder to let teacher/parents know about the students activity for addressing privacy concerns especially of minors involved in our programs.

in case you may not be aware of this please let us know at info@greencontributor.com

from Greencontributor