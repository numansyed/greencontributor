<header class="bg-light shadow-lg py-3">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-between">
            <div class="col-auto">
                <a href="{{ route('homepage') }}" class="navbar-brand"><img src="{{ asset('img/logo.png') }}" alt="{{ config('app.name') }}">GreenContributor</a>
            </div>
            <div class="col-auto row align-items-center justify-content-end">
                    <div class="col"><a href="{{ route('homepage') }}">Homepage</a></div>
                    <div class="col"><a href="{{ route('explore') }}">Explore</a></div>
                    <div class="col"><a href="{{ route('videos') }}">Videos</a></div>
                    <div class="col"><a href="{{ route('events') }}">Events</a></div>
                    <div class="col-auto"><a href="{{ route('shop') }}">Shop</a></div>
                    <div class="col"><a href="{{ route('about') }}">About</a></div>
                    <div class="col"><a href="{{ route('contact') }}">Contact</a></div>
                    <div class="col"></div>
                @guest
                    <div class="col"><a href="{{ route('login') }}">Login</a></div>
                    <div class="col"><a href="{{ route('register') }}">Register</a></div>
                @endguest
                @auth
                    <div class="col">
                        <a href="{{ route('dashboard') }}">Dashboard</a>
                    </div>
                    <div class="col">
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf
                            <button class="btn" type="submit" role="button">Logout</button>
                        </form>
                    </div>
                @endauth
            </div>
        </div>
    </div>
</header>
