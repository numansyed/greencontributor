@extends('front.layout')
@section('content')
    <!-- Slider Starts -->
    <section id="slider">
        <div id="homeSlider" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#homeSlider" data-slide-to="0" class="active"></li>
                <li data-target="#homeSlider" data-slide-to="1"></li>
                <li data-target="#homeSlider" data-slide-to="2"></li>
                <li data-target="#homeSlider" data-slide-to="3"></li>
                <li data-target="#homeSlider" data-slide-to="4"></li>
                <li data-target="#homeSlider" data-slide-to="5"></li>
                <li data-target="#homeSlider" data-slide-to="6"></li>
                <li data-target="#homeSlider" data-slide-to="7"></li>
                <li data-target="#homeSlider" data-slide-to="8"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="{{ asset('img/slider/1.jpeg') }}" alt="Slider 1">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('img/slider/2.jpeg') }}" alt="Slider 2">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('img/slider/3.jpeg') }}" alt="Slider 3">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('img/slider/4.jpeg') }}" alt="Slider 4">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('img/slider/5.jpeg') }}" alt="Slider 5">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('img/slider/6.jpeg') }}" alt="Slider 6">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('img/slider/7.jpeg') }}" alt="Slider 7">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('img/slider/8.jpeg') }}" alt="Slider 8">
                </div>
                <div class="carousel-item">
                    <img src="{{ asset('img/slider/9.jpeg') }}" alt="Slider 9">
                </div>
            </div>
            <a class="carousel-control-prev" href="#homeSlider" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#homeSlider" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </section>
    <!-- Slider Ends -->

    <!-- Event Starts -->
    <section id="event">
        <h2 class="text-center my-5">Event Display System</h2>
        <h4 class="text-center"><span class="font-weight-bolder">Your Local Time: </span><span class="localTime"></span></h4>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Country</th>
                                <th>City</th>
                                <th>Title</th>
                                <th>Destination Time</th>
                                <th>Status</th>
                                <th>Join</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($events as $event)
                                <tr class="{{ $loop->iteration % 2 ? 'table-primary' : 'table-active' }}">
                                    <td>{{ $event->webinarDate }}</td>
                                    <td>{{ $event->city->country }}</td>
                                    <td>{{ $event->city->name }}</td>
                                    <td>{{ $event->title }}</td>
                                    <td class="text-capitalize">
                                        @if(isset($event->webinarDate))
                                            {{ $event->webinarDate }}, {{ $event->start_time }}, {{ $event->city->timezone }}
                                        @endif
                                    </td>
                                    @if($event->status == 'tba' || $event->status == 'tbc')
                                        <td class="bg-warning text-uppercase">{{ $event->status }}</td>
                                    @elseif($event->status == 'confirmed' || $event->status == 'open')
                                        <td class="bg-success text-uppercase">{{ $event->status }}</td>
                                    @endif
                                    <td>
                                        @if(!empty($event->join_link))
                                            <a href="{{ $event->join_link }}" target="_blank" class="btn btn-primary text-light">Join</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
    <!-- Event Ends -->

    <!-- Videos Starts -->
    <section id="videos">
        <h2 class="text-center my-5">Videos</h2>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row">
                        @if(isset($videos) && count($videos)>0)
                            @foreach($videos as $video)
                                <div class="col-12">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <video src="{{ asset($video->link) }}" class="embed-responsive-item" controls="true" autoplay="true"></video>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Video Ends -->

    <!-- About Starts -->
    <section id="about">
        <h2 class="text-center my-5">About</h2>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-10">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{ asset($about ?? ''->image) }}" alt="About" class="img-fluid">
                        </div>
                        <div class="col-8">
                            <p>{{ $about ?? ''->text }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- About Ends -->


    {{--Video list section--}}
    <section id="video-list-section">
        <div class="container-fluid">
            <div class="page-content page-container" id="page-content">
                <div class="padding">
                    <div class="row  d-flex justify-content-center">
                        <div class="col-lg-12">
                            <div class="card">
                                <div class="card-body">
                                    <h4 class="card-title text-center my-5">video gallary</h4>
                                    <p class="card-text text-center"> Click on any image to open video </p>
                                    <div id="lightgallery" class="row lightGallery">
                                        <a href="https://www.youtube.com/watch?v=07d2dXHYb94" class="image-tile" data-abc="true"><img class="img-fluid" src="http://placehold.it/800x300" alt="image small">
                                            <div class="demo-gallery-poster"> <img src="http://www.urbanui.com/fily/template/images/lightbox/play-button.png" alt="image"> </div>
                                        </a> <a href="https://www.youtube.com/watch?v=YW3q59Kn42M" class="image-tile" data-abc="true"><img class="img-fluid" src="http://placehold.it/800x300" alt="image small">
                                            <div class="demo-gallery-poster"> <img src="http://www.urbanui.com/fily/template/images/lightbox/play-button.png" alt="image"> </div>
                                        </a> <a href="https://www.youtube.com/watch?v=y0yUXWjwYh4" class="image-tile" data-abc="true"><img class="img-fluid" src="http://placehold.it/800x300" alt="image small">
                                            <div class="demo-gallery-poster"> <img src="http://www.urbanui.com/fily/template/images/lightbox/play-button.png" alt="image"> </div>
                                        </a> <a href="https://www.youtube.com/watch?v=_EZbYkmkSoY" class="image-tile" data-abc="true"><img class="img-fluid" src="http://placehold.it/800x300" alt="image small">
                                            <div class="demo-gallery-poster"> <img src="http://www.urbanui.com/fily/template/images/lightbox/play-button.png" alt="image"> </div>
                                        </a> </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--Video section End--}}

    {{--Product slider start--}}
    <section id="productlist">
        <h2 class="text-center my-5">Product List</h2>
        <div class="container-fluid">

            <section>

                <div id="carousel-example-multi" class="carousel slide carousel-multi-item v-2 product-carousel" data-ride="carousel">

                    <!--Controls-->
                    <div class="controls-top my-3">
                        <a class="btn-floating btn-sm" href="#carousel-example-multi" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
                        <a class="btn-floating btn-sm" href="#carousel-example-multi" data-slide="next"><i class="fas fa-chevron-right"></i></a>
                    </div>
                    <!--/.Controls-->

                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#carousel-example-multi" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-multi" data-slide-to="1"></li>
                        <li data-target="#carousel-example-multi" data-slide-to="2"></li>
                        <li data-target="#carousel-example-multi" data-slide-to="3"></li>
                        <li data-target="#carousel-example-multi" data-slide-to="4"></li>
                        <li data-target="#carousel-example-multi" data-slide-to="5"></li>
                        <li data-target="#carousel-example-multi" data-slide-to="6"></li>
                        <li data-target="#carousel-example-multi" data-slide-to="7"></li>
                        <li data-target="#carousel-example-multi" data-slide-to="8"></li>
                    </ol>
                    <!--/.Indicators-->

                    <div class="carousel-inner" role="listbox">

                        <div class="carousel-item active mx-auto">
                            <div class="col-12 col-md-4 col-lg-2 mx-auto">
                                <div class="card mb-2">
                                    <div class="view overlay">
                                        <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Square/img(4).jpg" alt="Card image cap">
                                        <a href="#!">
                                            <div class="mask rgba-white-slight"></div>
                                        </a>
                                    </div>
                                    <div class="card-body p-3">
                                        <h5 class="card-title font-weight-bold fuchsia-rose-text mb-0">Moda</h5>
                                        <p class="aqua-sky-text mb-0">Plan B</p>
                                        <ul class="list-unstyled list-inline my-2">
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star-half-alt mimosa-text"></i></li>
                                        </ul>
                                        <p class="chili-pepper-text mb-0">9,99 $</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-12 col-md-4 col-lg-2 mx-auto">
                                <div class="card mb-2">
                                    <div class="view overlay">
                                        <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Square/img(1).jpg" alt="Card image cap">
                                        <a href="#!">
                                            <div class="mask rgba-white-slight"></div>
                                        </a>
                                    </div>
                                    <div class="card-body p-3">
                                        <h5 class="card-title font-weight-bold fuchsia-rose-text mb-0">Indie City</h5>
                                        <p class="aqua-sky-text mb-0">Pixies</p>
                                        <ul class="list-unstyled list-inline my-2">
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="far fa-star mimosa-text"></i></li>
                                        </ul>
                                        <p class="chili-pepper-text mb-0">14,99 $</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-12 col-md-4 col-lg-2 mx-auto">
                                <div class="card mb-2">
                                    <div class="view overlay">
                                        <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Square/img(7).jpg" alt="Card image cap">
                                        <a href="#!">
                                            <div class="mask rgba-white-slight"></div>
                                        </a>
                                    </div>
                                    <div class="card-body p-3">
                                        <h5 class="card-title font-weight-bold fuchsia-rose-text mb-0">Year</h5>
                                        <p class="aqua-sky-text mb-0">Indielectru</p>
                                        <ul class="list-unstyled list-inline my-2">
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                        </ul>
                                        <p class="chili-pepper-text mb-0">12,99 $</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-12 col-md-4 col-lg-2 mx-auto">
                                <div class="card mb-2">
                                    <div class="view overlay">
                                        <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Square/img(8).jpg" alt="Card image cap">
                                        <a href="#!">
                                            <div class="mask rgba-white-slight"></div>
                                        </a>
                                    </div>
                                    <div class="card-body p-3">
                                        <h5 class="card-title font-weight-bold fuchsia-rose-text mb-0">L'Hiver Indien</h5>
                                        <p class="aqua-sky-text mb-0">Baloji</p>
                                        <ul class="list-unstyled list-inline my-2">
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star-half-alt mimosa-text"></i></li>
                                        </ul>
                                        <p class="chili-pepper-text mb-0">10,99 $</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-12 col-md-4 col-lg-2 mx-auto">
                                <div class="card mb-2">
                                    <div class="view overlay">
                                        <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Square/img(6).jpg" alt="Card image cap">
                                        <a href="#!">
                                            <div class="mask rgba-white-slight"></div>
                                        </a>
                                    </div>
                                    <div class="card-body p-3">
                                        <h5 class="card-title font-weight-bold fuchsia-rose-text mb-0">Indie Funk</h5>
                                        <p class="aqua-sky-text mb-0">Generation Funk</p>
                                        <ul class="list-unstyled list-inline my-2">
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                        </ul>
                                        <p class="chili-pepper-text mb-0">19,99 $</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-12 col-md-4 col-lg-2 mx-auto">
                                <div class="card mb-2">
                                    <div class="view overlay">
                                        <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Square/img(2).jpg" alt="Card image cap">
                                        <a href="#!">
                                            <div class="mask rgba-white-slight"></div>
                                        </a>
                                    </div>
                                    <div class="card-body p-3">
                                        <h5 class="card-title font-weight-bold fuchsia-rose-text mb-0">Rockferry</h5>
                                        <p class="aqua-sky-text mb-0">Duffy</p>
                                        <ul class="list-unstyled list-inline my-2">
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star-half-alt mimosa-text"></i></li>
                                        </ul>
                                        <p class="chili-pepper-text mb-0">17,99 $</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-12 col-md-4 col-lg-2 mx-auto">
                                <div class="card mb-2">
                                    <div class="view overlay">
                                        <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Square/img(3).jpg" alt="Card image cap">
                                        <a href="#!">
                                            <div class="mask rgba-white-slight"></div>
                                        </a>
                                    </div>
                                    <div class="card-body p-3">
                                        <h5 class="card-title font-weight-bold fuchsia-rose-text mb-0">Female Indie Pop</h5>
                                        <p class="aqua-sky-text mb-0">Various artists</p>
                                        <ul class="list-unstyled list-inline my-2">
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="far fa-star mimosa-text"></i></li>
                                        </ul>
                                        <p class="chili-pepper-text mb-0">9,99 $</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-12 col-md-4 col-lg-2 mx-auto">
                                <div class="card mb-2">
                                    <div class="view overlay">
                                        <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Square/img(5).jpg" alt="Card image cap">
                                        <a href="#!">
                                            <div class="mask rgba-white-slight"></div>
                                        </a>
                                    </div>
                                    <div class="card-body p-3">
                                        <h5 class="card-title font-weight-bold fuchsia-rose-text mb-0">Rock 'N' Roll</h5>
                                        <p class="aqua-sky-text mb-0">Chuck Berry</p>
                                        <ul class="list-unstyled list-inline my-2">
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star-half-alt mimosa-text"></i></li>
                                        </ul>
                                        <p class="chili-pepper-text mb-0">29,99 $</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <div class="col-12 col-md-4 col-lg-2 mx-auto">
                                <div class="card mb-2">
                                    <div class="view overlay">
                                        <img class="card-img-top" src="https://mdbootstrap.com/img/Photos/Square/img(9).jpg" alt="Card image cap">
                                        <a href="#!">
                                            <div class="mask rgba-white-slight"></div>
                                        </a>
                                    </div>
                                    <div class="card-body p-3">
                                        <h5 class="card-title font-weight-bold fuchsia-rose-text mb-0">High Voltage</h5>
                                        <p class="aqua-sky-text mb-0">AC/DC</p>
                                        <ul class="list-unstyled list-inline my-2">
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                            <li class="list-inline-item mx-0"><i class="fas fa-star mimosa-text"></i></li>
                                        </ul>
                                        <p class="chili-pepper-text mb-0">24,99 $</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </section>

        </div>
    </section>
    {{--Product list end--}}
    <section id="peoplesaying">
        <h2 class="text-center my-5">What People are saying about us?</h2>
        <div class="container-fluid">
            <div class="d-flex justify-content-center py-3">
                <div class="mr-2">
                    <div class="card nm-card">
                        <div class="px-2 py-2"> <span class="maintxt">"Lorem ipsum dolor sit amet,consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore magna aliqua."</span>
                            <div class="d-flex pt-3">
                                <div><img src="https://i.imgur.com/hczKIze.jpg" width="50" class="rounded-circle"></div>
                                <div class="ml-2"> <span class="name">Dan Spratling</span>
                                    <p class="para">Company name</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card nm-card mt-3">
                        <div class="px-2 py-2"> <span class="maintxt">"Lorem ipsum dolor sit amet,consectetur adipiscing elit."</span>
                            <div class="d-flex pt-3">
                                <div><img src="https://i.imgur.com/hczKIze.jpg" width="50" class="rounded-circle"></div>
                                <div class="ml-2"> <span class="name">Dan Spratling</span>
                                    <p class="para">Company name</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="ml-2">
                    <div class="card nm-card">
                        <div class="px-2 py-2"> <span class="maintxt">"Lorem ipsum dolor sit amet,consectetur adipiscing elit."</span>
                            <div class="d-flex pt-3">
                                <div><img src="https://i.imgur.com/hczKIze.jpg" width="50" class="rounded-circle"></div>
                                <div class="ml-2"> <span class="name">Dan Spratling</span>
                                    <p class="para">Company name</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card nm-card mt-3">
                        <div class="px-2 py-2"> <span class="maintxt">"Lorem ipsum dolor sit amet,consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore magna aliqua."</span>
                            <div class="d-flex pt-3">
                                <div><img src="https://i.imgur.com/hczKIze.jpg" width="50" class="rounded-circle"></div>
                                <div class="ml-2"> <span class="name">Dan Spratling</span>
                                    <p class="para">Company name</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{--what they say about us end--}}
    <footer>
        <p class="text-center my-5"><i class="fas fa-copyright"></i>2020     All rights reserve by GreenContributor</p>
    </footer>
@endsection
