<?php

use App\Http\Controllers\CitiesController;
use App\Http\Controllers\SchoolsController;
use App\Http\Controllers\TeachersController;
use App\Http\Controllers\ActivityTypesController;
use App\Http\Controllers\ActivityController;
use App\Http\Controllers\WebinarController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\VideoCategoriesController;
use App\Http\Controllers\VideosController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PagesController::class, 'homepage'])->name('homepage');
Route::get('/explore', [PagesController::class, 'explore'])->name('explore');
Route::get('/videos', [PagesController::class, 'videos'])->name('videos');
Route::get('/events', [PagesController::class, 'events'])->name('events');
Route::get('/shop', [PagesController::class, 'shop'])->name('shop');
Route::get('/about', [PagesController::class, 'about'])->name('about');
Route::get('/contact', [PagesController::class, 'contact'])->name('contact');

Auth::routes();

Route::middleware('auth')->get('/dashboard', [App\Http\Controllers\PagesController::class, 'dashboard'])->name('dashboard');

Route::middleware('auth')->prefix('dashboard')->group(function(){
    Route::prefix('city')->group(function(){
        Route::get('/', [CitiesController::class, 'index'])->name('city.browse');
        Route::get('/create', [CitiesController::class, 'create'])->name('city.create');
        Route::post('/store', [CitiesController::class, 'store'])->name('city.store');
        Route::get('/edit/{id}', [CitiesController::class, 'edit'])->name('city.edit');
        Route::post('/update/{id}', [CitiesController::class, 'update'])->name('city.update');
        Route::get('/destroy/{id}', [CitiesController::class, 'destroy'])->name('city.destroy');
    });

    Route::prefix('school')->group(function(){
        Route::get('/', [SchoolsController::class, 'index'])->name('school.browse');
        Route::get('/create', [SchoolsController::class, 'create'])->name('school.create');
        Route::post('/store', [SchoolsController::class, 'store'])->name('school.store');
        Route::get('/edit/{id}', [SchoolsController::class, 'edit'])->name('school.edit');
        Route::post('/update/{id}', [SchoolsController::class, 'update'])->name('school.update');
        Route::get('/destroy/{id}', [SchoolsController::class, 'destroy'])->name('school.destroy');
    });

    Route::prefix('teacher')->group(function(){
        Route::get('/', [TeachersController::class, 'index'])->name('teacher.browse');
        Route::get('/create', [TeachersController::class, 'create'])->name('teacher.create');
        Route::post('/store', [TeachersController::class, 'store'])->name('teacher.store');
        Route::get('/edit/{id}', [TeachersController::class, 'edit'])->name('teacher.edit');
        Route::post('/update/{id}', [TeachersController::class, 'update'])->name('teacher.update');
        Route::get('/destroy/{id}', [TeachersController::class, 'destroy'])->name('teacher.destroy');
    });

    Route::prefix('activity-type')->group(function(){
        Route::get('/', [ActivityTypesController::class, 'index'])->name('activity-type.browse');
        Route::get('/create', [ActivityTypesController::class, 'create'])->name('activity-type.create');
        Route::post('/store', [ActivityTypesController::class, 'store'])->name('activity-type.store');
        Route::get('/edit/{id}', [ActivityTypesController::class, 'edit'])->name('activity-type.edit');
        Route::post('/update/{id}', [ActivityTypesController::class, 'update'])->name('activity-type.update');
        Route::get('/destroy/{id}', [ActivityTypesController::class, 'destroy'])->name('activity-type.destroy');
    });

    Route::prefix('activity')->group(function(){
        Route::get('/', [ActivityController::class, 'index'])->name('activity.browse');
        Route::get('/create', [ActivityController::class, 'create'])->name('activity.create');
        Route::post('/store', [ActivityController::class, 'store'])->name('activity.store');
        Route::get('/edit/{id}', [ActivityController::class, 'edit'])->name('activity.edit');
        Route::post('/update/{id}', [ActivityController::class, 'update'])->name('activity.update');
        Route::get('/destroy/{id}', [ActivityController::class, 'destroy'])->name('activity.destroy');
    });

    Route::prefix('webinar')->group(function(){
        Route::get('/', [WebinarController::class, 'index'])->name('webinar.browse');
        Route::get('/create', [WebinarController::class, 'create'])->name('webinar.create');
        Route::post('/store', [WebinarController::class, 'store'])->name('webinar.store');
        Route::get('/edit/{id}', [WebinarController::class, 'edit'])->name('webinar.edit');
        Route::post('/update/{id}', [WebinarController::class, 'update'])->name('webinar.update');
        Route::get('/destroy/{id}', [WebinarController::class, 'destroy'])->name('webinar.destroy');
    });

    Route::prefix('category')->group(function(){
        Route::get('/', [VideoCategoriesController::class, 'index'])->name('category.browse');
        Route::get('/create', [VideoCategoriesController::class, 'create'])->name('category.create');
        Route::post('/store', [VideoCategoriesController::class, 'store'])->name('category.store');
        Route::get('/edit/{id}', [VideoCategoriesController::class, 'edit'])->name('category.edit');
        Route::post('/update/{id}', [VideoCategoriesController::class, 'update'])->name('category.update');
        Route::get('/destroy/{id}', [VideoCategoriesController::class, 'destroy'])->name('category.destroy');
    });

    Route::prefix('video')->group(function(){
        Route::get('/', [VideosController::class, 'index'])->name('video.browse');
        Route::get('/create', [VideosController::class, 'create'])->name('video.create');
        Route::post('/store', [VideosController::class, 'store'])->name('video.store');
        Route::get('/edit/{id}', [VideosController::class, 'edit'])->name('video.edit');
        Route::post('/update/{id}', [VideosController::class, 'update'])->name('video.update');
        Route::get('/destroy/{id}', [VideosController::class, 'destroy'])->name('video.destroy');
    });
});
