<?php

namespace App\Http\Controllers;

use App\Models\About;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class AboutController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = About::first();
        return view('back.settings.about', compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $about = About::first();
        $about->title = $request->title;
        $about->text = $request->text;
        $about->mission = $request->mission;
        $about->vision = $request->vision;
        if($request->hasFile('image')){
            $about->image = 'storage/img/about/about.png';
            $request->file('image')->storeAs('public/img/about','about.png' );
        }
        try{
            $about->save();
            Session::flash('message', 'Successfully saved!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('settings.about');
        }catch(Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('settings.about');
        }
    }
}
