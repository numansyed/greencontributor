<?php

namespace App\Http\Controllers;

use App\Mail\SubmissionRecieved;
use App\Models\Activity;
use App\Models\ActivityType;
use App\Models\City;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\School;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class ActivityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if(Auth::user()->type == 'admin'){
            $activities = Activity::all();
            return view('back.activity.browse', compact('activities'));
        }else if(Auth::user()->type == 'teacher'){
            $activities = Activity::where('teacher_id', Auth::user()->id)->get();
            return view('back.activity.browse', compact('activities'));
        }else{
            $activities = Activity::where('student_id', Auth::user()->id)->get();
            return view('back.activity.browse', compact('activities'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $activityTypes = ActivityType::all();
        return view('back.activity.create', compact('activityTypes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $activity = new Activity();
        $student = Auth::user();
        $teacher = User::findOrFail($student->teacher_id);
        $city = User::findOrFail($student->city_id);
        $school = School::findOrFail($student->school_id);
        $activity->title = $request->title;
        $activity->activity_type_id = $request->activity_type_id;
        $activity->student_id = $student->id;
        $activity->teacher_id = $teacher->id;
        $activity->school_id = $school->id;
        $activity->city_id = $city->id;
        $activity->description = $request->description;
        $activity->video = $request->video;
        // handle Image
        if($request->hasFile('image')){
            $originalFileName = $request->file('image')->getClientOriginalName();
            $fileExt = $request->file('image')->getClientOriginalExtension();
            $originalFileNameWithoutExt = Str::of($originalFileName)->basename('.'.$fileExt);
            $fileNameToSave = $originalFileNameWithoutExt . '_' . time() . '.' . $fileExt;
            $activity->photo = 'storage/img/activity-photos/'.$fileNameToSave;
            $request->file('image')->storeAs('public/img/activity-photos/', $fileNameToSave);
        }
        try{
            $activity->save();
            Mail::to($teacher->email)->send(new SubmissionRecieved($student->name));
            Session::flash('message', 'Activity submitted successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('activity.browse');
        }catch (Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('activity.create')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activity = Activity::findOrFail($id);
        $activityTypes = ActivityType::all();
        $cities = City::all();
        $schools = School::all();
        $students = User::where('type', 'student')->get();
        return view('back.activity.edit', compact(['activityTypes', 'cities', 'schools', 'students', 'activity']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $activity = Activity::findOrFail($id);
        $student = Auth::user();
        $teacher = User::findOrFail($student->teacher_id);
        $city = User::findOrFail($student->city_id);
        $school = School::findOrFail($student->school_id);
        $activity->title = $request->title;
        $activity->activity_type_id = $request->activity_type_id;
        $activity->student_id = $student->id;
        $activity->teacher_id = $teacher->id;
        $activity->school_id = $school->id;
        $activity->city_id = $city->id;
        $activity->description = $request->description;
        $activity->video = $request->video;
        // handle Image
        if($request->hasFile('image')){
            $originalFileName = $request->file('image')->getClientOriginalName();
            $fileExt = $request->file('image')->getClientOriginalExtension();
            $originalFileNameWithoutExt = Str::of($originalFileName)->basename('.'.$fileExt);
            $fileNameToSave = $originalFileNameWithoutExt . '_' . time() . '.' . $fileExt;
            $activity->photo = 'storage/img/activity-photos/'.$fileNameToSave;
            $request->file('image')->storeAs('public/img/activity-photos/', $fileNameToSave);
        }
        try{
            $activity->save();
            Session::flash('message', 'Activity updated successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('activity.browse');
        }catch (Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('activity.edit', $id)->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activity = Activity::findOrFail($id);
        try{
            $activity->delete();
            Session::flash('message', 'Activity deleted successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('activity.browse');
        }catch (Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('activity.browse');
        }
    }
}
