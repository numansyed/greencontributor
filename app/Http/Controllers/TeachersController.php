<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\User;
use App\Models\School;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $teachers = User::where('type', 'teacher')->get();
        return view('back.teacher.browse', compact('teachers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();
        $schools = School::all();
        return view('back.teacher.create', compact(['cities', 'schools']));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $teacher = new User();
        $teacher->name = $request->name;
        $teacher->email = $request->email;
        $teacher->gender = $request->gender;
        $teacher->city_id = $request->city_id;
        $teacher->school_id = $request->school_id;
        $teacher->user_id = $request->user_id;
        if($request->has('password')){
            $teacher->password = Hash::make($request->password);
        }
        $teacher->type = 'teacher';
        try{
            $teacher->save();
            Session::flash('messages', 'Teacher added successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('teacher.browse');
        }catch(Exception $error){
            Session::flash('messages', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('teacher.create')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $teacher = User::findOrFail($id);
        $cities = City::all();
        $schools = School::all();
        return view('back.teacher.edit', compact(['teacher', 'cities', 'schools']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $teacher = User::findOrFail($id);
        $teacher->name = $request->name;
        $teacher->email = $request->email;
        $teacher->gender = $request->gender;
        $teacher->city_id = $request->city_id;
        $teacher->school_id = $request->school_id;
        $teacher->user_id = $request->user_id;
        $teacher->password = Hash::make($request->password);
        $teacher->type = 'teacher';
        try{
            $teacher->save();
            Session::flash('messages', 'Teacher updated successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('teacher.browse');
        }catch(Exception $error){
            Session::flash('messages', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('teacher.edit', $id)->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $teacher = User::findOrFail($id);
        try{
            $teacher->delete();
            Session::flash('messages', 'Teacher deleted successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('teacher.browse');
        }catch(Exception $error){
            Session::flash('messages', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('teacher.browse');
        }
    }
}
