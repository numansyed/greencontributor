<?php

namespace App\Http\Controllers;

use App\Models\ActivityType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class ActivityTypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $activityTypes = ActivityType::all();
        return view('back.activity-type.browse', compact('activityTypes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.activity-type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $activityType = new ActivityType();
        $activityType->title = $request->title;
        $activityType->points = $request->points;
        $activityType->hasVideo = $request->has('hasVideo');
        $activityType->hasPhoto = $request->has('hasPhoto');
        $activityType->hasText = $request->has('hasText');
        try{
            $activityType->save();
            Session::flash('message', 'Activity Master added successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('activity-type.browse');
        }catch (Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('activity-type.create')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $activityType = ActivityType::findOrFail($id);
        return view('back.activity-type.edit', compact('activityType'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $activityType = ActivityType::findOrFail($id);
        $activityType->title = $request->title;
        $activityType->points = $request->points;
        $activityType->hasVideo = $request->has('hasVideo');
        $activityType->hasPhoto = $request->has('hasPhoto');
        $activityType->hasText = $request->has('hasText');
        try{
            $activityType->save();
            Session::flash('message', 'Activity Master updated successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('activity-type.browse');
        }catch (Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('activity-type.edit', $id)->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $activityType = ActivityType::findOrFail($id);
        try{
            $activityType->delete();
            Session::flash('message', 'Activity Master deleted successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('activity-type.browse');
        }catch (Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('activity-type.browse');
        }
    }
}
