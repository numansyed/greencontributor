<?php

namespace App\Http\Controllers;

use App\Models\Video;
use App\Models\VideoCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;

class VideosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $videos = Video::all();
        return view('back.video.browse', compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = VideoCategory::all();
        return view('back.video.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if($request->hasFile('video')){
            $video = new Video();
            $video->title = $request->title;
            $video->description = $request->description;
            $video->video_category_id = $request->video_category_id;
            // handle file
            $originalFileName = $request->file('video')->getClientOriginalName();
            $fileExt = $request->file('video')->getClientOriginalExtension();
            $originalFileNameWithoutExt = Str::of($originalFileName)->basename('.'.$fileExt);
            $fileNameToSave = $originalFileNameWithoutExt . '_' . time() . '.' . $fileExt;
            $video->link = 'storage/videos/'.$fileNameToSave;
            try{
                $video->save();
                $request->file('video')->storeAs('public/videos', $fileNameToSave);
                Session::flash('message', 'Video added successfully!');
                Session::flash('alert-class', 'alert-success');
                return redirect()->route('video.browse');
            }catch (Exception $error){
                Session::flash('message', $error->getMessage());
                Session::flash('alert-class', 'alert-danger');
                return redirect()->route('video.create');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $video = Video::findOrFail($id);
        $categories = VideoCategory::all();
        return view('back.video.edit', compact(['video', 'categories']));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $video = Video::findOrFail($id);
        $video->title = $request->title;
        $video->description = $request->description;
        $video->video_category_id = $request->video_category_id;
        if($request->hasFile('video')){
            $originalFileName = $request->file('video')->getClientOriginalName();
            $fileExt = $request->file('video')->getClientOriginalExtension();
            $originalFileNameWithoutExt = Str::of($originalFileName)->basename('.'.$fileExt);
            $fileNameToSave = $originalFileNameWithoutExt . '_' . time() . '.' . $fileExt;
            $video->link = 'storage/videos/'.$fileNameToSave;
            $request->file('video')->storeAs('public/videos', $fileNameToSave);
        }
        try{
            $video->save();
            Session::flash('message', 'Video updated successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('video.browse');
        }catch (Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('video.edit', $id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $video = Video::findOrFail($id);
        try{
            $video->delete();
            Session::flash('message', 'Video deleted successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('video.browse');
        }catch (Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('video.browse');
        }
    }
}
