<?php

namespace App\Http\Controllers;

use App\Models\About;
use App\Models\Activity;
use App\Models\City;
use App\Models\HomepageSettings;
use App\Models\School;
use App\Models\User;
use App\Models\Slider;
use App\Models\Testimonial;
use App\Models\Webinar;
use App\Models\Video;
use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function homepage(){
        $sliders = Slider::all();
        $events = Webinar::paginate(5);
        $settings = HomepageSettings::first();
        $testimonials = Testimonial::all();
        $videos = Video::all();
        $about = About::first();
        return view('front.homepage', compact([
            'events',
            'videos',
            'about'
        ]));
    }

    public function explore(){
        $cities = City::all();
        return view('front.explore', compact('cities'));
    }

    public function videos(){
        $activities = Activity::all();
        return view('front.videos', compact('activities'));
    }

    public function events(){
        $events = Webinar::all();
        return view('front.events', compact('events'));
    }

    public function shop(){
        return view('front.shop');
    }

    public function about(){
        return view('front.about');
    }

    public function contact(){
        return view('front.contact');
    }

    public function register(){
        $schools = School::all();
        return view('back.register', [
            'schools' => $schools
        ]);
    }

    public function dashboard(){
        $cities = City::all();
        $schools = School::all();
        $students = User::where('type', 'student')->get();
        $teachers = User::where('type', 'teacher')->get();
        $activities = Activity::all();
        $webinars = Webinar::all();
        return view('back.dashboard', compact([
            'cities',
            'schools',
            'students',
            'teachers',
            'activities',
            'webinars'
        ]));
    }
}
