<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Session;

class CitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cities = City::orderBy('name', 'asc')->get();
        return view('back.city.browse', compact('cities'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('back.city.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $city = new City();
        $city->name = $request->name;
        $city->description = $request->description;
        $city->country = $request->country;
        $city->timezone = $request->timezone;
        // Handle File
        $originalFileName = $request->file('image')->getClientOriginalName();
        $fileExt = $request->file('image')->getClientOriginalExtension();
        $originalFileNameWithoutExt = Str::of($originalFileName)->basename('.'.$fileExt);
        $fileNameToSave = $originalFileNameWithoutExt . '_' . time() . '.' . $fileExt;
        $city->image = 'img/city/'.$fileNameToSave;
        try{
            $city->save();
            $request->file('image')->storeAs('public/img/city/', $fileNameToSave);
            Session::flash('message', 'City added successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('city.browse');
        }catch (Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('city.create')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $city = City::findOrFail($id);
        return view('back.city.edit', compact('city'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $city = City::findOrFail($id);
        $city->name = $request->name;
        $city->description = $request->description;
        $city->country = $request->country;
        $city->timezone = $request->timezone;
        if($request->hasFile('image')){
            $originalFileName = $request->file('image')->getClientOriginalName();
            $fileExt = $request->file('image')->getClientOriginalExtension();
            $originalFileNameWithoutExt = Str::of($originalFileName)->basename('.'.$fileExt);
            $fileNameToSave = $originalFileNameWithoutExt . '_' . time() . '.' . $fileExt;
            $city->image = 'img/city/'.$fileNameToSave;
            try{
                $city->save();
                $request->file('image')->storeAs('public/img/city/', $fileNameToSave);
                Session::flash('message', 'City updated successfully!');
                Session::flash('alert-class', 'alert-success');
                return redirect()->route('city.browse');
            }catch (Exception $error){
                Session::flash('message', $error->getMessage());
                Session::flash('alert-class', 'alert-danger');
                return redirect()->route('city.create')->withInput();
            }
        }else{
            try{
                $city->save();
                Session::flash('message', 'City updated successfully!');
                Session::flash('alert-class', 'alert-success');
                return redirect()->route('city.browse');
            }catch (Exception $error){
                Session::flash('message', $error->getMessage());
                Session::flash('alert-class', 'alert-danger');
                return redirect()->route('city.create')->withInput();
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $city = City::findOrFail($id);
        try {
            $city->delete();
            Session::flash('message', 'City deleted successfully!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('city.browse');
        } catch (Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('city.browse');
        }
    }
}
