<?php

namespace App\Http\Controllers;

use App\Models\Webinar;
use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class WebinarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $webinars = Webinar::all();
        return view('back.webinar.browse', compact('webinars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cities = City::all();
        return view('back.webinar.create', compact('cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $webinar = new Webinar();
        $webinar->title = $request->title;
        $webinar->description = $request->description;
        $webinar->city_id = $request->city_id;
        $webinar->start_time = $request->start_time;
        $webinar->end_time = $request->end_time;
        $webinar->webinarDate = $request->webinarDate;
        $webinar->status = $request->status;
        $webinar->join_link = $request->join_link;
        try{
            $webinar->save();
            Session::flash('message', 'Successfully added event.');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('webinar.browse');
        }catch(\Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('webinar.create')->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cities = City::all();
        $event = Webinar::findOrFail($id);
        return view('back.webinar.edit', compact([
            'cities',
            'event'
        ]));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $webinar = Webinar::findOrFail($id);
        $webinar->title = $request->title;
        $webinar->description = $request->description;
        $webinar->city_id = $request->city_id;
        $webinar->start_time = $request->start_time;
        $webinar->end_time = $request->end_time;
        $webinar->webinarDate = $request->webinarDate;
        $webinar->status = $request->status;
        $webinar->join_link = $request->join_link;
        try{
            $webinar->save();
            Session::flash('message', 'Successfully updated event.');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('webinar.browse');
        }catch(\Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('webinar.edit', $id)->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Webinar::findOrFail($id);
        try{
            $event->delete();
            Session::flash('message', 'Successfully deleted!');
            Session::flash('alert-class', 'alert-success');
            return redirect()->route('webinar.browse');
        }catch(Exception $error){
            Session::flash('message', $error->getMessage());
            Session::flash('alert-class', 'alert-danger');
            return redirect()->route('webinar.browse');
        }
    }
}
