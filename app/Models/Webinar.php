<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Webinar extends Model
{
    use HasFactory;

    public function city(){
        return $this->belongsTo(City::class);
    }

    public function webinar(){
        return $this->belongsTo(User::class);
    }
}
