<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    public function users(){
        return $this->hasMany(User::class);
    }

    public function schools(){
        return $this->hasMany(School::class);
    }

    public function activities(){
        return $this->hasMany(Activity::class);
    }

    public function webinars(){
        return $this->hasMany(Webinar::class);
    }
}
